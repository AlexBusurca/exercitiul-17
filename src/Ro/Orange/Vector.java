/**In a main class called Vector read a number n and a vector with n whole number components.
 Show up the content of the vector components.
 This should be displayed in your console:
 Introduce the number of elements n = 3
 “V[0]=1
 V[1]=4
 V[2]=6
 1
 4
 6”*/


package Ro.Orange;

import java.io.IOException;
import java.util.Scanner;

public class Vector {
    public static void main(String[] args) {

        //folosesc un obiect de tip scanner pentru a interpreta corect input-ul de la user
        //daca as folosi direct System.in ar trebui sa transform din ASCII in int
        // cu obiectul de tip scanner nu mai e nevoie de aceasta transformare
        Scanner input = new Scanner(System.in);

        //rog userul sa introduca dimensiunea array-ului
        System.out.println("Set the number of elements");

        //stochez raspunsul lui intr-o variabila si ii afisez numarul de elemente
        int n = input.nextInt();
        System.out.println("The number of elements is " + n);

        //folosesc inputul userului pentru a declara si seta dimensiunea initiala a array-ului
        int[] vector = new int[n];
        System.out.println("Please enter one value at a time for each of the " + n + " elements and press enter.");

        //setez elementele array-ului pe baza inputului de la user si afisez valoarea elementelor
                for (int i=0; i<n;i++) {
                int valElement = input.nextInt();
                vector[i] = valElement;
                System.out.println("V[" + i + "] = " + vector[i]);
                }

        //afisez elementele setate de user
                for (int i = 0; i<n;i++) {
                    System.out.println(vector[i]);
                }


    }
}
